<?php

namespace App\Controller;

use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(TypeRepository $typeRepository)
    {

        return $this->render('default/index.html.twig', [
            'types' => $typeRepository->findAll()
        ]);
    }
}
