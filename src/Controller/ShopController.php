<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Type;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\TypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    /**
     * @Route("/{type_slug}/", name="type")
     */
    public function type(Type $type)
    {
        return $this->render('shop/type.html.twig', [
            'type' => $type,
        ]);
    }

    /**
     * @Route("/{type_slug}/{category_slug}/", name="category")
     */
    public function category(Type $type, Category $category)
    {
        return $this->render('shop/category.html.twig', [
            'category' => $category
        ]);
    }

    /**
     * @Route("/{type_slug}/{category_slug}/{product_slug}/", name="product")
     */
    public function product(Type $type, Category $category, Product $product)
    {
        return $this->render('shop/product.html.twig', [
            'product' => $product
        ]);
    }

}
